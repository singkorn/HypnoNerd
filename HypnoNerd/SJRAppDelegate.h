//
//  SJRAppDelegate.h
//  HypnoNerd
//
//  Created by Singkorn Junko Dhepyasuvan on 7/18/14.
//  Copyright (c) 2014 Debaya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SJRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
