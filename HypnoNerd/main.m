//
//  main.m
//  HypnoNerd
//
//  Created by Singkorn Junko Dhepyasuvan on 7/18/14.
//  Copyright (c) 2014 Debaya. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SJRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SJRAppDelegate class]));
    }
}
